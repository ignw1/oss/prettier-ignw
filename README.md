# prettier-ignw

IGNW standard prettier config.

## Usage

To use this prettier config in your project

- install the prettier npm dependency `npm install prettier`
- install this package as a dev dependency `npm install prettier-ignw`
- configure your editor to use the prettier plugin
- add

```JSON
    "prettier": prettier-ignw
```

as a top level key to your projects `package.json`

To use but allow for overriding this prettier configuration (NOT RECOMMENDED),
create a `.prettierrc.js` in your project and add

```javascript
module.exports = {
  ...require("prettier-ignw"),
  semi: false, // overide specific rules
};
```

## Development

Add / update [prettier rules](https://prettier.io/docs/en/options.html) as needed.

## Publishing

Checkout a branch, code and commit changes, get code reviewed and approved.

Once the code is approved update the packages version (using semver style) by running

```bash
npm version X.Y.Z
```

Log into to your npm account

```bash
npm login
```

Then publish the package to npm for public consumpation:

```bash
npm publish
```

## How this package works

[See prettier docs for how sharing of prettier config works](https://prettier.io/docs/en/configuration.html#sharing-configurations)
